% Simscape(TM) Multibody(TM) version: 7.0

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(31).translation = [0.0 0.0 0.0];
smiData.RigidTransform(31).angle = 0.0;
smiData.RigidTransform(31).axis = [0.0 0.0 0.0];
smiData.RigidTransform(31).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [49.999999999999986 -335 149.99999999999991];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[headdelta-1:-:delta l1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [2.6047430878861633e-10 -1.5812151588079359e-10 -29.999999999999954];  % mm
smiData.RigidTransform(2).angle = 1.5700924586837752e-16;  % rad
smiData.RigidTransform(2).axis = [0.2461259174918623 -0.96923786179595195 -1.8727635625208865e-17];
smiData.RigidTransform(2).ID = 'F[headdelta-1:-:delta l1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [104.90381056766529 -335 -118.30127018922238];  % mm
smiData.RigidTransform(3).angle = 0.52359877559830359;  % rad
smiData.RigidTransform(3).axis = [-3.9581206761708282e-16 1 -1.0605752387249127e-16];
smiData.RigidTransform(3).ID = 'B[headdelta-1:-:delta l1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [2.3717916519672144e-10 8.2009066204591363e-11 -29.999999999999623];  % mm
smiData.RigidTransform(4).angle = 2.8180255245614219e-15;  % rad
smiData.RigidTransform(4).axis = [0.60601415321371854 -0.79545386170705068 -6.7922347661577881e-16];
smiData.RigidTransform(4).ID = 'F[headdelta-1:-:delta l1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-154.903810567666 -335 -31.698729810776594];  % mm
smiData.RigidTransform(5).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(5).axis = [-0.96592582628906953 2.7449707054615069e-17 -0.25881904510251624];
smiData.RigidTransform(5).ID = 'B[headdelta-1:-:delta l1-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [1.6319390283570101e-11 7.4546147033061061e-11 -29.999999999998593];  % mm
smiData.RigidTransform(6).angle = 3.1742026598034022e-15;  % rad
smiData.RigidTransform(6).axis = [-0.43474431227583188 0.90055393116893001 -6.2136715180487253e-16];
smiData.RigidTransform(6).ID = 'F[headdelta-1:-:delta l1-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [282.50000000000006 -30.000000000000139 60];  % mm
smiData.RigidTransform(7).angle = 0;  % rad
smiData.RigidTransform(7).axis = [0 0 0];
smiData.RigidTransform(7).ID = 'B[delta l1-1:-:deltal2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [5.4313886721502058e-11 395.00000000046794 3.7827874166396214e-10];  % mm
smiData.RigidTransform(8).angle = 0;  % rad
smiData.RigidTransform(8).axis = [0 0 0];
smiData.RigidTransform(8).ID = 'F[delta l1-1:-:deltal2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [30.000000000000028 0 -54.999999999999936];  % mm
smiData.RigidTransform(9).angle = 0;  % rad
smiData.RigidTransform(9).axis = [0 0 0];
smiData.RigidTransform(9).ID = 'B[deltahinge-1:-:deltal2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [1.1031886515411315e-10 -394.99999999988501 -1.4250645108404569e-10];  % mm
smiData.RigidTransform(10).angle = 0;  % rad
smiData.RigidTransform(10).axis = [0 0 0];
smiData.RigidTransform(10).ID = 'F[deltahinge-1:-:deltal2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [282.50000000000006 -30.000000000000139 -20.000000000000018];  % mm
smiData.RigidTransform(11).angle = 0;  % rad
smiData.RigidTransform(11).axis = [0 0 0];
smiData.RigidTransform(11).ID = 'B[delta l1-1:-:deltal2-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [-2.4701307665964123e-10 395.00000000045202 3.0314595278468914e-10];  % mm
smiData.RigidTransform(12).angle = 0;  % rad
smiData.RigidTransform(12).axis = [0 0 0];
smiData.RigidTransform(12).ID = 'F[delta l1-1:-:deltal2-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [-30 0 -54.999999999999936];  % mm
smiData.RigidTransform(13).angle = 0;  % rad
smiData.RigidTransform(13).axis = [0 0 0];
smiData.RigidTransform(13).ID = 'B[deltahinge-1:-:deltal2-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [1.723066134218243e-10 -394.99999999990433 -1.7905676941154525e-11];  % mm
smiData.RigidTransform(14).angle = 0;  % rad
smiData.RigidTransform(14).axis = [0 0 0];
smiData.RigidTransform(14).ID = 'F[deltahinge-1:-:deltal2-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [282.50000000000006 -29.999999999999972 59.999999999999943];  % mm
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0 0 0];
smiData.RigidTransform(15).ID = 'B[delta l1-3:-:deltal2-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [3.6791902857657988e-11 395.00000000033913 8.9457330432196613e-11];  % mm
smiData.RigidTransform(16).angle = 0;  % rad
smiData.RigidTransform(16).axis = [0 0 0];
smiData.RigidTransform(16).ID = 'F[delta l1-3:-:deltal2-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [32.631397208144676 0 53.480762113532919];  % mm
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0 0 0];
smiData.RigidTransform(17).ID = 'B[deltahinge-1:-:deltal2-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [-2.6308555334253469e-10 -394.99999999964263 -5.8761884247360285e-10];  % mm
smiData.RigidTransform(18).angle = 0;  % rad
smiData.RigidTransform(18).axis = [0 0 0];
smiData.RigidTransform(18).ID = 'F[deltahinge-1:-:deltal2-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [282.5 -29.999999999999915 -19.999999999999908];  % mm
smiData.RigidTransform(19).angle = 0;  % rad
smiData.RigidTransform(19).axis = [0 0 0];
smiData.RigidTransform(19).ID = 'B[delta l1-2:-:deltal2-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [9.9475983006414026e-13 394.99999999986767 1.7246293282369152e-10];  % mm
smiData.RigidTransform(20).angle = 0;  % rad
smiData.RigidTransform(20).axis = [0 0 0];
smiData.RigidTransform(20).ID = 'F[delta l1-2:-:deltal2-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [-32.631397208143817 0 53.480762113533366];  % mm
smiData.RigidTransform(21).angle = 0;  % rad
smiData.RigidTransform(21).axis = [0 0 0];
smiData.RigidTransform(21).ID = 'B[deltahinge-1:-:deltal2-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [1.7649881556280889e-11 -395.00000000033975 -5.3570659019896993e-10];  % mm
smiData.RigidTransform(22).angle = 0;  % rad
smiData.RigidTransform(22).axis = [0 0 0];
smiData.RigidTransform(22).ID = 'F[deltahinge-1:-:deltal2-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [282.50000000000006 -29.999999999999972 -20.000000000000018];  % mm
smiData.RigidTransform(23).angle = 0;  % rad
smiData.RigidTransform(23).axis = [0 0 0];
smiData.RigidTransform(23).ID = 'B[delta l1-3:-:deltal2-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [5.5734972193022259e-11 395.00000000031298 6.049560852261493e-11];  % mm
smiData.RigidTransform(24).angle = 0;  % rad
smiData.RigidTransform(24).axis = [0 0 0];
smiData.RigidTransform(24).ID = 'F[delta l1-3:-:deltal2-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [62.6313972081442 1.1102230246251565e-13 1.5192378864663514];  % mm
smiData.RigidTransform(25).angle = 0;  % rad
smiData.RigidTransform(25).axis = [0 0 0];
smiData.RigidTransform(25).ID = 'B[deltahinge-1:-:deltal2-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [-4.0745362639427185e-10 -394.99999999966485 -4.3709746933018323e-10];  % mm
smiData.RigidTransform(26).angle = 0;  % rad
smiData.RigidTransform(26).axis = [0 0 0];
smiData.RigidTransform(26).ID = 'F[deltahinge-1:-:deltal2-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [282.5 -30.000000000000028 60.000000000000057];  % mm
smiData.RigidTransform(27).angle = 0;  % rad
smiData.RigidTransform(27).axis = [0 0 0];
smiData.RigidTransform(27).ID = 'B[delta l1-2:-:deltal2-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [-1.8215473573945928e-10 394.99999999987438 5.3987037063052412e-11];  % mm
smiData.RigidTransform(28).angle = 0;  % rad
smiData.RigidTransform(28).axis = [0 0 0];
smiData.RigidTransform(28).ID = 'F[delta l1-2:-:deltal2-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [-62.631397208144115 0 1.5192378864671285];  % mm
smiData.RigidTransform(29).angle = 0;  % rad
smiData.RigidTransform(29).axis = [0 0 0];
smiData.RigidTransform(29).ID = 'B[deltahinge-1:-:deltal2-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [5.0803805606847163e-10 -395.00000000034044 -1.1269207789155189e-10];  % mm
smiData.RigidTransform(30).angle = 0;  % rad
smiData.RigidTransform(30).axis = [0 0 0];
smiData.RigidTransform(30).ID = 'F[deltahinge-1:-:deltal2-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [380.37435814446656 733.75698670406075 533.52158099533926];  % mm
smiData.RigidTransform(31).angle = 0;  % rad
smiData.RigidTransform(31).axis = [0 0 0];
smiData.RigidTransform(31).ID = 'RootGround[headdelta-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(4).mass = 0.0;
smiData.Solid(4).CoM = [0.0 0.0 0.0];
smiData.Solid(4).MoI = [0.0 0.0 0.0];
smiData.Solid(4).PoI = [0.0 0.0 0.0];
smiData.Solid(4).color = [0.0 0.0 0.0];
smiData.Solid(4).opacity = 0.0;
smiData.Solid(4).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.12746075709740667;  % kg
smiData.Solid(1).CoM = [0.10914594527246854 0 -5.8373428005204817e-06];  % mm
smiData.Solid(1).MoI = [8148.246109784589 2.2119693625329462 8148.1829051533505];  % kg*mm^2
smiData.Solid(1).PoI = [-0.00022036824409246689 -5.5105038754769429e-06 0];  % kg*mm^2
smiData.Solid(1).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'deltal2*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.64688441203851466;  % kg
smiData.Solid(2).CoM = [114.79225214698114 -1.8215258598961848 17.957666893411261];  % mm
smiData.Solid(2).MoI = [210.45085578867545 7327.3459454238109 7305.4381445573235];  % kg*mm^2
smiData.Solid(2).PoI = [2.4065085864634241 -151.64984633382861 195.92495268173528];  % kg*mm^2
smiData.Solid(2).color = [1 1 1];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'delta l1*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.20739711244002043;  % kg
smiData.Solid(3).CoM = [0 7.3451007955703567 2.696334620165187e-07];  % mm
smiData.Solid(3).MoI = [172.5688814034319 277.15410252920793 172.5688410888296];  % kg*mm^2
smiData.Solid(3).PoI = [3.8981148684936871e-07 0 0];  % kg*mm^2
smiData.Solid(3).color = [0.89803921568627454 0.91764705882352937 0.92941176470588238];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'deltahinge*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 21.074753114606807;  % kg
smiData.Solid(4).CoM = [0.000579101777173087 -305.1204694378236 -0.00026691846492600036];  % mm
smiData.Solid(4).MoI = [610765.40913338773 1189876.5678090176 610752.72761360498];  % kg*mm^2
smiData.Solid(4).PoI = [-0.013652192538751209 -0.16400902450279703 0.031244616682079954];  % kg*mm^2
smiData.Solid(4).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'headdelta*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(3).Rz.Pos = 0.0;
smiData.RevoluteJoint(3).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -118.49676341912246;  % deg
smiData.RevoluteJoint(1).ID = '[headdelta-1:-:delta l1-1]';

smiData.RevoluteJoint(2).Rz.Pos = -47.257369654654759;  % deg
smiData.RevoluteJoint(2).ID = '[headdelta-1:-:delta l1-2]';

smiData.RevoluteJoint(3).Rz.Pos = 151.15501257324127;  % deg
smiData.RevoluteJoint(3).ID = '[headdelta-1:-:delta l1-3]';


%Initialize the SphericalJoint structure array by filling in null values.
smiData.SphericalJoint(12).S.Pos.Angle = 0.0;
smiData.SphericalJoint(12).S.Pos.Axis = [0.0 0.0 0.0];
smiData.SphericalJoint(12).ID = '';

smiData.SphericalJoint(1).S.Pos.Angle = 116.11705666278843;  % deg
smiData.SphericalJoint(1).S.Pos.Axis = [0.035312132218598143 -0.99002108312135195 0.13642326888548487];
smiData.SphericalJoint(1).ID = '[delta l1-1:-:deltal2-1]';

smiData.SphericalJoint(2).S.Pos.Angle = 31.627812675032267;  % deg
smiData.SphericalJoint(2).S.Pos.Axis = [-0.41820283922707735 -0.75077541335546294 0.51131464282117445];
smiData.SphericalJoint(2).ID = '[deltahinge-1:-:deltal2-1]';

smiData.SphericalJoint(3).S.Pos.Angle = 69.12097079142498;  % deg
smiData.SphericalJoint(3).S.Pos.Axis = [-0.054434429215391933 -0.97416651387532172 0.21917229788455778];
smiData.SphericalJoint(3).ID = '[delta l1-1:-:deltal2-2]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(4).S.Pos.Angle = 32.43028796473638;  % deg
smiData.SphericalJoint(4).S.Pos.Axis = [-0.58710809345503889 0.73740784380197599 0.33396670267993767];
smiData.SphericalJoint(4).ID = '[deltahinge-1:-:deltal2-2]';

smiData.SphericalJoint(5).S.Pos.Angle = 27.809316755140468;  % deg
smiData.SphericalJoint(5).S.Pos.Axis = [0.31945490095233381 0.8417208520868551 0.43526379750642397];
smiData.SphericalJoint(5).ID = '[delta l1-3:-:deltal2-3]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(6).S.Pos.Angle = 23.735816202881804;  % deg
smiData.SphericalJoint(6).S.Pos.Axis = [0.86377140106829009 -0.35203855428415204 -0.36051050330614109];
smiData.SphericalJoint(6).ID = '[deltahinge-1:-:deltal2-3]';

smiData.SphericalJoint(7).S.Pos.Angle = 87.521532092100827;  % deg
smiData.SphericalJoint(7).S.Pos.Axis = [-0.13409591605828428 0.9829597386873431 0.12573160865987429];
smiData.SphericalJoint(7).ID = '[delta l1-2:-:deltal2-4]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(8).S.Pos.Angle = 72.086461884234851;  % deg
smiData.SphericalJoint(8).S.Pos.Axis = [0.47951837720177248 -0.84323853340621202 0.24292159579732531];
smiData.SphericalJoint(8).ID = '[deltahinge-1:-:deltal2-4]';

smiData.SphericalJoint(9).S.Pos.Angle = 28.407980554909297;  % deg
smiData.SphericalJoint(9).S.Pos.Axis = [0.25566342256571278 0.86951707543371759 0.42258285565221476];
smiData.SphericalJoint(9).ID = '[delta l1-3:-:deltal2-5]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(10).S.Pos.Angle = 22.254643121779822;  % deg
smiData.SphericalJoint(10).S.Pos.Axis = [0.86497247000226896 -0.30335758796037066 -0.39974591926002195];
smiData.SphericalJoint(10).ID = '[deltahinge-1:-:deltal2-5]';

smiData.SphericalJoint(11).S.Pos.Angle = 163.15369334722865;  % deg
smiData.SphericalJoint(11).S.Pos.Axis = [-0.1261823871248931 0.99172193677714626 0.023782457704148323];
smiData.SphericalJoint(11).ID = '[delta l1-2:-:deltal2-6]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(12).S.Pos.Angle = 38.931584812314377;  % deg
smiData.SphericalJoint(12).S.Pos.Axis = [0.36786026452532083 0.31584504758303522 0.8745974683822445];
smiData.SphericalJoint(12).ID = '[deltahinge-1:-:deltal2-6]';

